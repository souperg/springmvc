<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<script language="javascript">     

 var currentDate = new Date(<%=new java.util.Date().getTime()%>);   

   function run() {       
		currentDate.setSeconds(currentDate.getSeconds() + 1);
		document.getElementById("currentTime").innerHTML = currentDate.toLocaleString();
   }     
   window.setInterval("run();", 1000); 

 </script>

  
<body>
	<p align="center">欢迎来到SpringMVC</p>
	<p align="center">服务器当前时间：</p>
	<div align="center" id="currentTime"></div>  
</body>
</html>